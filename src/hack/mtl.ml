let rec int_power a = function
  | 0 -> 1
  | 1 -> a
  | n -> 
    let b = int_power a (n / 2) in
    b * b * (if n mod 2 = 0 then 1 else a)

let hex_sfmt () n =
  if n < 16
  then Format.sprintf "0x0%x" n
  else Format.sprintf "0x%x" n


let rec code_gen acc exp =
  if exp = 0 then (
    (string_of_int acc)
  ) else (
    let power = int_power 2 (exp - 1) in
    let upper = code_gen (acc + power) (exp - 1) in
    let lower = code_gen (acc) (exp - 1) in
    Format.sprintf "if b >= %a then (%s) else (%s)"
      hex_sfmt (acc + power) upper lower
  )
