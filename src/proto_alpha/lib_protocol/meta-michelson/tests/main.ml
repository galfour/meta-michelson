let () =
  Printexc.record_backtrace true ;
  Format.printf "Before test\n%!" ;
  Alcotest.run "test" [
    Heap.main ;
    Coase.main ;
  ] ;
  ()
