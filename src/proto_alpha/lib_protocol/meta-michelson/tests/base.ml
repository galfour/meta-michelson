module Env = struct
  let environment =
    Meta_michelson_helpers.Error.force_lwt ~msg:"generating environment" @@
    Meta_michelson_helpers.Misc.init_environment ()

  let identities = environment.identities
  let tezos_context = environment.tezos_context
end

module Run = Meta_michelson_helpers.Contract.Step(Env)

let test name f =
  Alcotest_lwt.test_case name `Quick @@ fun _sw () ->
  f () >>= function
  | Ok () -> Lwt.return_unit
  | Error errs ->
    Tezos_stdlib_unix.Logging_unix.close () >>= fun () ->
    Format.eprintf "Alcotest-error, json errors:\n" ;
    List.iter Meta_michelson_helpers.Error.print_error errs ;
    Lwt.fail Alcotest.Test_error
