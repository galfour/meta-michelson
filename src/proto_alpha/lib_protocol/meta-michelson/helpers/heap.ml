open Contract
open Proto_alpha
open Script_typed_ir
open Michelson_wrap
open Michelson_modules

module type COMPARABLE_TY = sig
  type t
  val ty : t ty
  val cmp : (t * (t * 'r), int_num * 'r) code
  val compare : t -> t -> int
end

module type KEY_TY = sig
  type t
  val ty : t ty
  val raw_to_int : t -> int
  val raw_of_int : int -> t

  val to_nat : (t * 'r, nat * 'r) code
  val of_nat : (nat * 'r, t * 'r) code
end

module LinkedHeap (Index:KEY_TY) (V:COMPARABLE_TY) : sig
  type index = Index.t
  type value = V.t
  type iv = index * value
  type t
  val ty : t ty

  module Bimap : module type of KV_Big_bimap(Index)(V)

  val empty : t
  val get_iv_v : (value * (t * 'r), (index * value) * 'r) code
  val raw_to_list : t -> iv list
  val raw_of_list : iv list -> t
  val raw_to_string : (V.t -> string) -> t -> string
  val raw_make : (Bimap.t * nat) -> t
  val raw_unmake : t -> (Bimap.t * nat)

  val assert_iv : msg:string -> t -> iv -> unit
  val assert_i : msg:string -> t -> index -> unit
  val assert_size : msg:string -> t -> int -> unit
  val assert_heap_property : msg:string -> t -> unit
  
  val size : (t * 'r, nat * 'r) code
  val insert : (value * (t * 'r), t * 'r) code
  val pop : (t * 'r, value * (t * 'r)) code
  val get : (t * 'r, value * 'r) code
  val update_increase : (value * (value * (t * 'r)), t * 'r) code
end = struct
  type index = Index.t
  type value = V.t
  type iv = index * value
  let iv_ty = Types.(pair Index.ty V.ty)

  module Bimap = KV_Big_bimap(Index)(V)

  type t = Bimap.t * nat
  let ty = Types.(pair Bimap.ty nat)
  
  let empty : t = Values.(Bimap.empty, nat 0)
  let raw_make x = x
  let raw_unmake x = x
  let raw_size (lh:t) : int =
    Option.unopt_exn (Failure "Linked Heap : raw_size")
      (AC.Script_int.to_int @@ snd lh)
  let raw_get_i = Bimap.raw_get_k
  let raw_get_v = Bimap.raw_get_v
  let raw_mem_i = Bimap.raw_mem_k
  let raw_mem_v = Bimap.raw_mem_v
  let raw_set_i = Bimap.raw_set_k
  
  let raw_to_list (lh:t) : iv list =
    let ns = 1 -- raw_size lh in
    let bm = fst lh in
    List.map (fun i -> (Index.raw_of_int i, raw_get_i bm (Index.raw_of_int i))) ns

  let raw_of_list (lst:iv list) : t =
    (List.fold_left (fun b (index, value) ->
        raw_set_i b index value
       ) (fst empty) lst, Values.nat (List.length lst))

  let raw_to_string (f:value -> string) (bm:t) : string =
    let lst = raw_to_list bm in
    let converse = List.map (fun (_, v) -> (v, raw_get_v (fst bm) v)) lst in
    let open Format in
    fprintf str_formatter "#%d:\t" (Option.unopt_exn (Failure "") @@ AC.Script_int.to_int @@ snd bm) ;
    List.iter (fun ((i, v), (v2, i2)) ->
        fprintf str_formatter "(%d : %s)|(%s : %d)\t"
          (Index.raw_to_int i) (f v)
          (f v2) (Index.raw_to_int i2)
      ) (List.combine lst converse) ;
    flush_str_formatter ()

  let raw_to_string (f:value -> string) (bm:t) : string =
    let lst = raw_to_list bm in
    let open Format in
    fprintf str_formatter "#%d:\t" (Option.unopt_exn (Failure "") @@ AC.Script_int.to_int @@ snd bm) ;
    List.iter (fun (i, v) ->
        fprintf str_formatter "(%d : %s)\t"
          (Index.raw_to_int i) (f v)
      ) lst ;
    flush_str_formatter ()

  let assert_iv ~msg (bm:t) iv = Bimap.assert_kv ~msg (fst bm) iv
  let assert_i ~msg (bm:t) iv = Bimap.assert_k ~msg (fst bm) iv
  let assert_size ~msg (bm:t) s = if snd bm <> Values.nat s then raise @@ Failure ("assert_size : " ^ msg)
  let assert_heap_property ~msg (lh:t) =
    let bm = fst lh in
    let size = raw_size lh in
    for i = 1 to size do
      let value = raw_get_i bm (Index.raw_of_int i) in
      if (i * 2 <= size) then (
        let left_child = raw_get_i bm @@ Index.raw_of_int (i * 2) in
        let cmp = V.compare value left_child in
        if cmp > 0
        then raise (Failure ("Assert heap property : " ^ msg))
      ) ;
      if (i * 2 + 1 <= size) then (
        let right_child = raw_get_i bm @@ Index.raw_of_int (i * 2 + 1) in
        let cmp = V.compare value right_child in
        if cmp > 0
        then raise (Failure ("Assert heap property : " ^ msg))
      ) ;
    done ;
    ()
    
  
  let increment_size : (t * 'r, t * 'r) code  = fun s -> !: (s <. dup <: car <: dip (cdr @| add_n 1) <: pair)
  let decrement_size : (t * 'r, t * 'r) code  = fun s -> !: (s <. dup <: car <: dip (cdr @| sub_n 1 @| force_nat) <: pair)
  let get_i : (index * (t * 'r), value * 'r) code = fun s -> !: (s <. dip car <: Bimap.get_k)
  let get_i_opt = fun s -> !: (s <. dip car <: Bimap.get_k_opt)
  let get_v = fun s -> !: (s <. dip car <: Bimap.get_v)

  let set_i (type r) : (index * (value * (t * r)), t * r) code = fun s -> !: (
      s <.
      diip unpair <:
      Bimap.set_k <:
      pair
    )

  let del_i (type r) : (index * (t * r), t * r) code = fun s -> !: (s <. dip (dup @| carcdr) <: Bimap.del_k <: pair)
  let del_v (type r) : (value * (t * r), t * r) code = fun s -> !: (s <. dip (dup @| carcdr) <: Bimap.del_v <: pair)
  let del_iv (type r) : (iv * (t * r), t * r) code = fun s -> !: (s <. car <: del_i)
  let set_v = fun s -> !: (s <. diip (dup @| carcdr) <: Bimap.set_v <: pair)
  let set_iv = fun s -> !: (s <. unpair <: set_i)
  let get_iv_i (type r) : (index * (t * r), iv * r) code = fun s -> !: (s <. keep1 get_i <: pair)
  let get_iv_i_keep (type r) : (index * (t * r), iv * (t * r)) code = fun s -> !: (s <. keep2 get_i <: dip swap <: pair)
  let get_iv_v (type r) : (value * (t * r), iv * r) code = fun s -> !: (s <. keep1 get_v <: swap <: pair)
  let get_iv_i_opt (type r) : (index * (t * r), iv option * r) code = fun s ->
    !: (s <.
        keep1 get_i_opt <:
        swap <:
        if_none
        ~target:(Types.option iv_ty @: tail @@ tail s)
          (drop @| push_none iv_ty)
          (swap @| pair @| some)
       )
  let make : (_, iv * 'r) code = pair
  let unmake : (iv * 'r, _) code = unpair
  let unmake_key : (iv * 'r, index * 'r) code = car

  let swap_iv : (iv * (iv * (t * 'r)), t * 'r) code = fun s -> !: (
      s <.
      keep2 (carcdr @| set_i) <:
      cdrcar <: swap <: set_i
    )
  let size : (t * 'r, nat * 'r) code = cdr

  let zero_index (type r) : (index * (iv * (t * r)), (iv, unit) union * (t * r)) code =
    fun s -> !: (s <. drop <: drop <: push_unit <: right iv_ty)

  let stop_branch (type r) : (iv * (iv * (t * r)), (iv, unit) union * (t * r)) code = fun s ->
    !: (s <. drop <: drop <: push_unit <: right iv_ty)

  let swap_branch (type r) : (iv * (iv * (t * r)), (iv, unit) union * (t * r)) code = fun s ->
    !: (s <.
        keep2 swap_iv <: carcdr <: make <: left Types.unit)

  let succ_index (type r) : (index * (iv * (t * r)), (iv, unit) union * (t * r)) code = fun s ->
    !: (s <.
        swap <: dip get_iv_i_keep <: swap <:
        keep2 (cdrcdr @| V.cmp) <: bubble_2 <:
        gt <: if_bool swap_branch stop_branch
       )

  let insert (type r) : (value * (t * r), t * r) code =
    let pre_loop (type r) : (value * (t * r), iv * (t * r)) code =
      fun s -> !: (
          s <.
          dip increment_size <:
          dip (dup @| size) <: swap <: Index.of_nat <: make <:
          keep1 set_iv
        ) in
    let main_loop (type r) : (iv * (t * r), unit * (t * r)) code =
      fun s -> !: (
          s <.
          left Types.unit <: loop_left ~after:(Types.unit @: tail s) (fun s -> !: (
              s <.
              dup <: unmake_key <: Index.to_nat <: div_n 2 <: dup <: gt_nat <:
              dip Index.of_nat <:
              if_bool succ_index zero_index
            ))) in
    fun s -> !: (
        s <.
        pre_loop <:
        main_loop <:
        drop
      )

  let get_first_element (type r) : (t * r, iv * r) code = fun s ->
    !: (s <. push_nat 1 <: Index.of_nat <: keep1 get_i <: pair)

  let get_last_element (type r) : (t * r, iv * r) code = fun s ->
    !: (s <. dup <: cdr <: add_n 1 <: Index.of_nat <: keep1 get_i <: pair)

  let get_children_index (type r) : (iv * r, index * (index * r)) code = fun s ->
    !: (s <.
        car <: Index.to_nat <:
        push_nat 2 <: mul_natnat <: keep1 (push_nat 1 @| add_natnat) <:
        Index.of_nat <: dip Index.of_nat
       )

  let get_children (type r) : (index * (index * (t * r)), iv option * (iv option * r)) code = fun s ->
    !: (s <. dip (dip dup @| get_iv_i_opt) <: swap <: dip get_iv_i_opt <: swap)

  let children_to_list (type r) : (iv option * (iv option * r), iv list * r) code = fun s ->
    !: (s <. cons_nil iv_ty <: swap <: if_none noop cons_list <: swap <: if_none noop cons_list)

  let get_children_list (type r) : (iv * (t * r), (iv list) * r) code = fun s ->
    !: (s <. get_children_index <: get_children <: children_to_list)

  let compare_iv (type r) : (iv * (iv * r), int_num * r) code = fun s ->
    !: (s <. cdr <: dip cdr <: V.cmp)

  let loop_content (type r) : (iv * (t * r), _) code =
    let aux (type r) : (iv * ((bool * iv) * r), (bool * iv) * r) code =
      fun s -> !: (
          s <.
          dip (unpair @| swap) <: (* child | parent *)
          keep2 compare_iv <: bubble_2 <:
          lt <: (* child is smaller *)
          if_bool
            (fun s -> !: (s <. dip drop <: swap <: drop <: push_bool true <: pair))
            (fun s -> !: (s <. drop <: swap <: pair))
        ) in
    fun s -> !: (
        s <.
        dip dup <: keep1 get_children_list <:
        dup <: dip (push_bool false @| pair @| swap) <:
        dip (list_iter aux) <:
        swap <: unpair <: (* a child is smaller *)
        if_bool
          (fun s -> !: (
               s <.
               keep2 swap_iv <:
               carcdr <: pair <:
               left Types.unit))
          (fun s -> !: (s <. drop <: drop <: push_unit <: right iv_ty ))
      )
  
  let pop (type r) : (t * r, value * (t * r)) code =
    let pop_first_element (type r) : (t * r, value * (t * r)) code = fun s -> !: (
        s <.
        keep1 get_first_element <:
        swap <:
        keep1 del_iv <:
        cdr <:
        dip decrement_size
      ) in
    let move_last_element (type r) : (t * r, iv * (t * r)) code = fun s -> !: (
        s <. dup <: get_last_element <:
        cdr <: keep1 del_v <: push_nat 1 <: Index.of_nat <: make <:
        keep1 set_iv
      ) in
    let pre_percolation (type r) : (t * r, bool * (value * (t * r))) code = fun s -> !: (
        s <.
        dup <: size <: assert_positive_nat <:
        eq_n 1 <:
        dip pop_first_element
      ) in
    let percolation (type r) : (t * r, t * r) code = fun s -> !: (
        s <.
        move_last_element <:
        left Types.unit <:
        loop_left loop_content <: drop
      ) in
    fun s -> !: (
        s <.
        pre_percolation <:
        if_bool ~target:(stack V.ty s) noop (dip percolation)
      )

  let get (type r) : (t * r, value * r) code = fun s -> !: (
      s <.
      push_nat 1 <: Index.of_nat <: get_i
    )
  
  let update_increase (type r) : (value * (value * (t * r)), t * r) code =
    let get_old_iv = fun s -> !: (
        s <. diip dup <: dip swap <: get_iv_v
      ) in
    let remove_old_iv = fun s -> !: (
        s <. car <: dip swap <: keep1 del_i
      ) in
    let set_new_iv = fun s -> !: (
        s <. dip swap <: keep2 set_i
      ) in
    fun s -> !: (     (* Old value | New value *)
        s <.
        get_old_iv <:
        remove_old_iv <:
        set_new_iv <:
        make <: left Types.unit <:
        loop_left loop_content <: drop
      )
(*
     Map : indice -> (value)
     Map : handle(value) -> indice
  *)
  

end

module type Heap = sig
  type t
  type index = int
  type value
  val cmp : value -> value -> bool
  val insert : t -> value -> t
  val update : t -> value -> value -> t
  val get : t -> value
  val pop : t -> t
end
