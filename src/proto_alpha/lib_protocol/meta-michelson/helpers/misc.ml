module Test_helpers = Tezos_alpha_test_helpers
module Proto_alpha = Tezos_alpha_test_helpers.Proto_alpha

open Error

type identity = {
  public_key_hash : Signature.public_key_hash;
  public_key : Signature.public_key;
  secret_key : Signature.secret_key;
  implicit_contract : Proto_alpha.Alpha_context.Contract.t;
}

type environment = {
  tezos_context : Proto_alpha.Alpha_context.t ;
  identities : identity list ;
}

let init_environment () =
  Test_helpers.Context.init_extended 10 >>=? fun (blk, contracts, accounts) ->
  Test_helpers.Incremental.begin_construction blk >>=? fun i ->
  let tezos_context = Test_helpers.Incremental.context i in
  let tezos_context = Proto_alpha.Alpha_context.Gas.set_limit tezos_context @@ Z.of_int 350000 in
  let identities =
    List.map (fun ((a:Test_helpers.Account.t), c) -> {
          public_key = a.pk ;
          public_key_hash = a.pkh ;
          secret_key = a.sk ;
          implicit_contract = c ;
        }) @@
    List.combine accounts contracts in
  return {tezos_context ; identities}

let contextualize ~msg ?environment f =
  let lwt =
    let environment = match environment with
      | None -> init_environment ()
      | Some x -> return x in
    environment >>=? f
  in
  force_ok ~msg @@ Lwt_main.run lwt 
