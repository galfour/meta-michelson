module Test_helpers = Tezos_alpha_test_helpers
module Proto_alpha = Tezos_alpha_test_helpers.Proto_alpha

let print_error err =
  let json = Error_monad.json_of_error err in
  Format.printf "%s\n" @@ Data_encoding.Json.to_string json

let force_ok ?(msg = "") = function
  | Ok x -> x
  | Error errs ->
    Format.printf "Errors :\n";
    List.iter print_error errs ;
    raise @@ Failure ("force_ok : " ^ msg)

let force_ok_str ?(msg = "") = function
  | Ok x -> x
  | Error err ->
    Format.printf "Error : %s\n" err;
    raise @@ Failure ("force_ok : " ^ msg)

open Proto_alpha

let (>>??) = Alpha_environment.Error_monad.(>>?)

let alpha_wrap a = Proto_alpha.Alpha_environment.wrap_error a

let force_ok_alpha ~msg a = force_ok ~msg @@ alpha_wrap a

let force_lwt ~msg a = force_ok ~msg @@ Lwt_main.run a

let force_lwt_alpha ~msg a = force_ok ~msg @@ alpha_wrap @@ Lwt_main.run a

let assert_error () = function
  | Ok _ -> fail @@ failure "assert_error"
  | Error _ -> return ()

let (>>=??) a f =
  a >>= fun a ->
  match alpha_wrap a with
  | Ok result -> f result
  | Error errs -> Lwt.return (Error errs)


